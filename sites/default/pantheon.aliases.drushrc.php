<?php
  /**
   * Pantheon drush alias file, to be placed in your ~/.drush directory or the aliases
   * directory of your local Drush home. Once it's in place, clear drush cache:
   *
   * drush cc drush
   *
   * To see all your available aliases:
   *
   * drush sa
   *
   * See http://helpdesk.getpantheon.com/customer/portal/articles/411388 for details.
   */

  $aliases['calc-barrel.live'] = array(
    'uri' => 'live-calc-barrel.pantheonsite.io',
    'db-url' => 'mysql://pantheon:e63891607af8473680c1f71eb960a407@dbserver.live.fea386ba-028f-476c-a540-64306e0288a5.drush.in:21064/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.live.fea386ba-028f-476c-a540-64306e0288a5.drush.in',
    'remote-user' => 'live.fea386ba-028f-476c-a540-64306e0288a5',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['calc-barrel.test'] = array(
    'uri' => 'test-calc-barrel.pantheonsite.io',
    'db-url' => 'mysql://pantheon:34a4811fd8c4400d85fbf7f5388e81f8@dbserver.test.fea386ba-028f-476c-a540-64306e0288a5.drush.in:19592/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.test.fea386ba-028f-476c-a540-64306e0288a5.drush.in',
    'remote-user' => 'test.fea386ba-028f-476c-a540-64306e0288a5',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'files',
      '%drush-script' => 'drush',
     ),
  );
  $aliases['calc-barrel.dev'] = array(
    'uri' => 'dev-calc-barrel.pantheonsite.io',
    'db-url' => 'mysql://pantheon:04933b3b84de4d359313b855103c6c3b@dbserver.dev.fea386ba-028f-476c-a540-64306e0288a5.drush.in:19588/pantheon',
    'db-allows-remote' => TRUE,
    'remote-host' => 'appserver.dev.fea386ba-028f-476c-a540-64306e0288a5.drush.in',
    'remote-user' => 'dev.fea386ba-028f-476c-a540-64306e0288a5',
    'ssh-options' => '-p 2222 -o "AddressFamily inet"',
    'path-aliases' => array(
      '%files' => 'files',
      '%drush-script' => 'drush',
     ),
  );
